/**
 * Created by phangty on 17/10/16.
 */
// init your app to let angular recognize the tags on your html.
var RegApp = angular.module("BindingApp", ['color.picker']);

(function() {
    var RegCtrl = function() {
        var ctrl = this;

        ctrl.myname="Roger";
        ctrl.age = 20;
        ctrl.color = "";
        ctrl.var4 = (4+3) + ' ' + (2+2);
        ctrl.style = {
            "background-color": "pink"
        };
        // model
        ctrl.person = {
            firstname: "Kenneth",
            lastName: "Phang"
        }
        console.log(ctrl.color);

        ctrl.changeColour = function (){
            console.log("change colour");
            ctrl.style['background-color'] = ctrl.color;
        }
    };
    RegApp.controller("RegCtrl", [RegCtrl]);
}) ();