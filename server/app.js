//Load express
var express = require("express");
//Create an instance of express application
var app = express();
/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */
console.log(__dirname + "/../client");
app.use(express.static(__dirname + "/../client"));
app.use("/bower_components", express.static(__dirname + "../client/bower_components"));

/*
app.get('/',function (req,res,next) {
    res.sendFile(__dirname + "../client")

})*/

//Start the web server on port 3000
app.listen(3000, function() {
    console.info("Webserver started on port 3000");
});